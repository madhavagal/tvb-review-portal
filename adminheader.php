<?php
session_start();
$connect = mysqli_connect("localhost", "root", "", "tvb");
$id=$_SESSION['id'];
$query1 = "SELECT name,email from admin WHERE id=$id";
$result1 = mysqli_query($connect, $query1);
$row1 = mysqli_fetch_array($result1);
$name = $row1['name'];
$email = $row1['email'];
if($email != $_SESSION['email']){
   echo '<script>alert("Acess denied. Log in again!")</script>';
   echo "<script> location.href='login.php'; </script>";
       exit;
}
?>
<?php
  if(isset($_POST['signout'])){
    session_destroy();
	 unset($_SESSION['email']);
    header('location: login.php');
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-grid.min.css">
  <!--  Make sure your always using the latest version of Bootstrap here-->
  <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
  <script href="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="teachdash.css">
  <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Neuton&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Archivo+Narrow&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet">
  <script src="jquery-3.4.1.min.js"></script>

</head>

<body class="stdbody">
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark navcustom fixed-top">
    <!-- Just an image -->

    <a class="navbar-brand" href="#">
      <img src="assets/logo.png" width="80px" height="50px">
    </a>


    <div class="collapse navbar-collapse justify-content-end collapsecustom" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <li class="nav-item"><a class="nav-link" href="admindashboard.php">Home</a></li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php echo $name; ?>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
            <form method="post" action="admindashboard.php">
              <button type="submit" class="dropdown-item" name="signout">Sign Out</button>
              <hr>
              <button type="button" class="dropdown-item" name="uppass" data-toggle="modal" data-target="#updatepassmodal">Update Password</button>
              <hr>
              <a class="dropdown-item" href="changelog.php">View Changelog</a>
            </form>
          </div>
        </li>
      </ul>
    </div>
  </nav>
  <br><br><br><br>
    <script src="bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>
