<?php
  include("adminheader.php");
?>

<?php
  $connect = mysqli_connect("localhost", "root", "", "tvb");
  $query1 = "SELECT name,id,photo FROM course";
  $result1 = mysqli_query($connect, $query1);
  $dyncourses = '<table cellpadding="20">';
  $i = 0;
  while($row1 = mysqli_fetch_array($result1)){
    $cname = $row1['name'];
    $cphoto = $row1['photo'];
    $cimg = '<img src="data:image/jpeg;base64,'.base64_encode($cphoto).'" height="150px" width="319px" class="card-img-top" alt="No Photo Available"/>';
    $cid = $row1['id'];
    $coursediv = '<div class="card border-dark mb-3" style="width: 18rem;">
                    '.$cimg.'
                    <div class="card-body">
                      <center><h5 class="card-title">'.$cname.'</h5></center>
                      <hr style="height:1px;border:none;color:#333;background-color: #333; margin:10px;">
                      <center><a href="admcourse.php?cid='.$cid.'" class="btn btn-primary">View Details</a></center>
                    </div>
                  </div>';
      if ($i % 4 == 0) { // if $i is divisible by our target number (in this case "3")
          $dyncourses .= "<tr><td>" . $coursediv . "</td>";
      } else {
        $dyncourses .= "<td>" . $coursediv . "</td>";
      }
      $i++;
 }
$dyncourses .= "</tr></table>";
?>

<?php
  $connect = mysqli_connect("localhost", "root", "", "tvb");
  $query4= "SELECT name,id,photo FROM teacher";
  $result4 = mysqli_query($connect, $query4);
  $dynteacher = '<table cellpadding="20">';
  $i = 0;
  while($row4 = mysqli_fetch_array($result4)){
    $tname = $row4['name'];
    $tphoto = $row4['photo'];
    if(empty($tphoto))
    {
      $timg = '<img src="assets/default.png" height="150px" width="150px" alt="No Photo Available" style="margin:10px; border: 1px solid black;"/>';
    }
    else{
      $timg = '<img src="data:image/jpeg;base64,'.base64_encode($tphoto).'" height="150px" width="150px" alt="No Photo Available" style="margin:10px; border: 1px solid black;" /> ';
    }
    $tid = $row4['id'];
    $teacherdiv = '<div class="card border-dark mb-3" style="width: 18rem;">
                    <center>'.$timg.'</center>
                    <div class="card-body">
                      <center><h5 class="card-title">'.$tname.'</h5></center>
                      <hr style="height:1px;border:none;color:#333;background-color: #333; margin:10px;">
                      <center><a href="adminstr.php?tid='.$tid.'" class="btn btn-primary">View Profile</a></center>
                    </div>
                  </div>';
      if ($i % 4 == 0) { // if $i is divisible by our target number (in this case "3")
          $dynteacher .= "<tr><td>" . $teacherdiv . "</td>";
      } else {
        $dynteacher .= "<td>" . $teacherdiv . "</td>";
      }
      $i++;
 }
$dynteacher .= "</tr></table>";
?>

<?php
  if(isset($_POST['addcourse'])){
  $adminid = $_SESSION['id'];
  $cname = $_POST['cname'];
  $cinfo = $_POST['cinfo'];
  $cduration = $_POST['cduration'];
  $file = addslashes(file_get_contents($_FILES["cimage"]["tmp_name"]));  
  $query2 = "INSERT INTO course(name,duration,info,photo) VALUES ('$cname','$cduration','$cinfo','$file')";
  $query3 = "INSERT INTO changelog(adminid,comment) VALUES ($adminid,'Added course $cname')";
  if(mysqli_query($connect,$query2))
  {
   mysqli_query($connect,$query3);
//   echo '<script>alert("Success");</script>';
    header("Refresh:0");
    $_POST["addcourse"] = '';
  }
  else{
    echo '<script>alert("FAil");</script>';
  } 
  }

?>
<?php
  $query5 = "SELECT id,name FROM course";
  $result5 = mysqli_query($connect, $query5);
  $courseoptn = '<option value="0">Select a course</option>';
  while($row5 = mysqli_fetch_array($result5)){
    $cid = $row5['id'];
    $cname = $row5['name'];
    $courseoptn .= '<option value="'.$cid.'">'.$cname.'</option>';
  }
  
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-grid.min.css">
  <!--  Make sure your always using the latest version of Bootstrap here-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
  <script href="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="admindash.css">
  <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Neuton&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Archivo+Narrow&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet">
  <script src="jquery-3.4.1.min.js"></script>

</head>

<body class="stdbody">

  <div class="container-fluid">
    <div class="row">
      <div class="col-6">
        <div class="row">
          <div class="col-4">
            <button type="button" class="btn btn-primary btn-lg custbtn" data-toggle="modal" data-target="#addstudentmodal" style="margin-left:20px;padding-left: 30px;padding-right: 30px;">Add Student</button>
          </div>
          <div class="col-4">
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addadminmodal" style="margin-left:20px;padding-left: 30px;padding-right: 30px;">Add Admin</button>
          </div>
          <div class="col-4">
            <a href="viewstudents.php" class="btn btn-primary btn-lg" style="margin-left:20px;padding-left: 30px;padding-right: 30px;">View Students</a>
          </div>
        </div>
      </div>
    </div>
    <br>

    <p class="custpara" style="margin-bottom:0px;">View Reviews - </p>
    <div class="fluid-container review">
      <form id="review" action="admnreviews.php" method="post">
        <div class="row">
          <div class="col-6">
            <div class="row">
              <div class="col-4"><select class="form-control" id="course" name="course">
                  <?php echo $courseoptn; ?>
                </select></div>
              <div class="col-4"><select class="form-control" id="batch" name="batch">
                </select></div>
              <div class="col-4"><button type="submit" name="viewreviews" id="go" class="btn btn-success">Go</button></div>
            </div>
          </div>
        </div>
      </form>
    </div>

    <br><br>
    <p class="custpara" style="margin-bottom:0px;">Courses Offered - </p>
    <div class="container-fluid courses">
      <button class="btn btn-success" style="margin-left:30px;" data-toggle="modal" data-target="#addcoursemodal"> <i class="fa fa-plus-circle" style="font-size:20px;"></i> Add New Course</button>
      <div class="divscroll"><?php echo $dyncourses; ?></div>
    </div>
    <br><br>
    <p class="custpara" style="margin-bottom:0px;">Faculty - </p>

    <div class="container-fluid teachers">
      <button class="btn btn-success" style="margin-left:30px;" data-toggle="modal" data-target="#addteachermodal"> <i class="fa fa-plus-circle" style="font-size:20px;"></i> Add New Instructor</button>
      <div class="divscroll"><?php echo $dynteacher; ?></div>
    </div>
    <br><br>


    <!--    <br><br><br><br><br><br>-->



  </div>

  <!--Modals  -->
  <div class="modal fade" id="addstudentmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Student</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p id="addserrors"></p>
          <form id="addstudentform">
            <div class="form-group">
              <label for="exampleInputEmail1">Name</label>
              <input type="text" name="sname" class="form-control" id="sname" placeholder="Enter Student Name" required>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" name="semail" class="form-control" id="semail" placeholder="Enter Student Email" required>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Phone number</label>
              <input type="tel" name="sphno" class="form-control" id="sphone" placeholder="Enter Student Phone Number" required>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="addadminmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Admin</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p id="addaerrors"></p>
          <form id="addadminform">
            <div class="form-group">
              <label for="exampleInputEmail1">Name</label>
              <input type="text" name="aname" class="form-control" id="aname" placeholder="Enter Admin Name" required>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" name="aemail" class="form-control" id="aemail" placeholder="Enter Admin Email" required>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Phone number</label>
              <input type="tel" name="aphno" class="form-control" id="aphone" placeholder="Enter Admin Phone Number" required>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="addcoursemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Course</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p id="addcerrors"></p>
          <form id="addcourseform" method="post" action="admindashboard.php" enctype="multipart/form-data">
            <div class="form-group">
              <label for="exampleInputEmail1">Course Name</label>
              <input type="text" name="cname" class="form-control" id="cname" placeholder="Enter Course Name" required>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Duration(in Weeks)</label>
              <input type="number" min="1" name="cduration" class="form-control" id="cduration" required>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Info</label>
              <textarea name="cinfo" class="form-control" id="cinfo" placeholder="Enter Course Info" required></textarea>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Select Course Image</label>
              <input type="file" class="form-control-file" id="cimage" name="cimage" required>
            </div>
            <button type="submit" class="btn btn-primary" name="addcourse">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="addteachermodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Instructor</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p id="addterrors"></p>
          <form id="addteacherform">
            <div class="form-group">
              <label for="exampleInputEmail1">Instructor Name</label>
              <input type="text" name="tname" class="form-control" id="tname" placeholder="Enter Instructor Name" required>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" name="temail" class="form-control" id="temail" placeholder="Enter Instructor Email" required>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Phone number</label>
              <input type="tel" name="tphno" class="form-control" id="tphone" placeholder="Enter Instructor Phone Number" required>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>


  <!--  <script src="bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js"></script>-->
  <script>
    $(document).ready(function() {
      $("#addstudentform").submit(function(e) {
        //          alert("form submitted");
        e.preventDefault();
        var form = $(this);
        var url = "addstudent.php";
        $.ajax({
          type: "POST",
          url: url,
          data: form.serialize(),
          success: function(data) {
            var result = $.trim(data);
            $("#addserrors").replaceWith(data);
          }

        });

      });

    });

  </script>
  <script>
    $(document).ready(function() {
      $("#addadminform").submit(function(e) {
        //          alert("form submitted");
        e.preventDefault();
        var form = $(this);
        var url = "addadmin.php";
        $.ajax({
          type: "POST",
          url: url,
          data: form.serialize(),
          success: function(data) {
            var result = $.trim(data);
            $("#addaerrors").replaceWith(data);
          }

        });

      });

    });

  </script>
  <script>
    $(document).ready(function() {
      $("#addteacherform").submit(function(e) {
        //          alert("form submitted");
        e.preventDefault();
        var form = $(this);
        var url = "addteacher.php";
        $.ajax({
          type: "POST",
          url: url,
          data: form.serialize(),
          success: function(data) {
            var result = $.trim(data);
            $("#addterrors").replaceWith(data);
          }

        });

      });

    });

  </script>
  <script>
    $(document).ready(function() {
      $("#course").change(function() {
        var course = $("#course").val();
        //        alert(course);
        var url = "selectbatch.php";
        $.ajax({
          type: "POST",
          url: url,
          data: {
            cid: course
          },
          success: function(data) {
            var result = $.trim(data);
            $("#batch").html(data);
          }

        });

      });
    });

  </script>
  <script>
    $(document).ready(function() {
      $("#review").submit(function(e) {
        var course = $("#course").val();
        var batch = $("#batch").val();
        if (course == 0 || batch == 0) {
          alert("Please select course and batch");
          e.preventDefault();
        }

      });
    });

  </script>
  
</body>

</html>
