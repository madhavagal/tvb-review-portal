<?php
session_start();
$connect = mysqli_connect("localhost", "root", "", "tvb");
$id=$_SESSION['id'];
$query1 = "SELECT name,email,photo from teacher WHERE id=$id";
$result1 = mysqli_query($connect, $query1);
$row1 = mysqli_fetch_array($result1);
$name = $row1['name'];
$email = $row1['email'];
$photo = $row1['photo'];
$profilepic = '<img src="data:image/jpeg;base64,'.base64_encode($photo ).'" width="150" height = "190" id="profile_pic" alt="Your profile pic here" />';
if($email != $_SESSION['email']){
   echo '<script>alert("Acess denied. Log in again!")</script>';
   echo "<script> location.href='login.php'; </script>";
       exit;
}
?>
<?php
  if(isset($_POST['signout'])){
    session_destroy();
	 unset($_SESSION['email']);
    header('location: login.php');
  }
?>
<?php
  if(isset($_POST['updatepass'])){
    
    $oldpass = $_POST['oldpass'];
    $newpass = $_POST['newpass'];
    $query2 = "SELECT password FROM teacher WHERE id = $id";
    $result2 = mysqli_query($connect, $query2);
    $row2 = mysqli_fetch_array($result2);
    $curpass = $row2['password'];
    if($oldpass == $newpass and $oldpass == $curpass ){
      echo "<script>alert('New password can not be same as old password')</script>";
    } 
    elseif($oldpass != $curpass){
      echo "<script>alert('Enter the correct password')</script>";
    }
	 else{
        $query3 = "UPDATE teacher SET `password` = '$newpass' WHERE id = $id";
        if(mysqli_query($connect, $query3) == TRUE){
          echo "<script>alert('Password updated successfully')</script>";
        }
        else{
          echo "<script>alert('Please try again')</script>";
        }
      }
    }
?>
<?php 
  if(isset($_POST["updatepropic"]))  
 {  
      $file = addslashes(file_get_contents($_FILES["profilepic"]["tmp_name"]));  
//      $query = "INSERT INTO tbl_images(name) VALUES ('$file')";
      $query4 = "UPDATE teacher SET `photo` = '$file' WHERE id = $id";
      if(mysqli_query($connect, $query4))  
      {  
           echo '<script>alert("Profile ")</script>';  
      }  
 }  
?>
<?php 
  $query5 = "SELECT teachercourse.courseid, teachercourse.batchid FROM teachercourse INNER JOIN batches ON teachercourse.courseid = batches.courseid AND teachercourse.batchid = batches.batchid WHERE teachercourse.teacherid = $id AND batches.active = 1;";
  $dynpage = '';
  $dynmodals = '';
  $result5 = mysqli_query($connect, $query5);
  $j  = 0;
  while($row5 = mysqli_fetch_array($result5)){
    $j++;
    $dyndiv = '<div class="coursecontainer">';
    $bid = $row5['batchid'];
    $cid = $row5['courseid'];
//    echo $cid.'  '.$bid;
    $query8 = "SELECT name FROM course WHERE id = $cid";
    $result8 = mysqli_query($connect,$query8);
    $row8 = mysqli_fetch_array($result8);
    $cname = $row8['name'];
    $dyndiv .= '<div class="toggle">
                  <div class="row ">
                  <div class="col-9 clickable">
                  <h4 class="batchhead">'.$cname.' - Batch '.$bid.'</h4>
            </div>
            <div class="col-3"> <button class="btn btn-success addctopic name="addtopic" data-toggle="modal" data-target="#addtopicmodal'.$j.'">Add topic for upcoming class</button></div>
          </div>
        </div>';
    
    
    $modal = '';
    $query11 = "SELECT classno,classdate,topic FROM courseclass WHERE courseid = $cid AND batchid = $bid AND teacherid = $id AND classdate >= CURRENT_DATE ORDER BY classdate ASC LIMIT 1";
    $result11 = mysqli_query($connect, $query11);
     if(mysqli_num_rows($result11) == 0){
       $modal = '  <div class="modal fade" id="addtopicmodal'.$j.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add topic for upcoming class</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h6>Upcoming class not added yet</h6>
      </div>
    </div>
  </div>
</div>';
     }
     else{
      $row11 = mysqli_fetch_array($result11);
     $classno = $row11['classno'];
     $update = date("d-m-Y",strtotime($row11['classdate']));
     $topic = $row11['topic'];
    $modal = '<div class="modal fade" id="addtopicmodal'.$j.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add topic for upcoming class</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h6>Week '.$classno.', '.$update.' :</h6>
        <h6 class="topicadded">Topic added - '.$topic.'</h6>
        <form class="addctopicform">
        <div class="form-group">
          <input type="hidden" name="courseid" value="'.$cid.'">
          <input type="hidden" name="batchid" value="'.$bid.'">
          <input type="hidden" name="teacherid" value="'.$id.'">
          <input type="hidden" name="classno" value="'.$classno.'">
          <input type="text" name="topic" placeholder="Enter the topic" style="width:100%" required>
          </div>
          <button type="submit" name="submittopic" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>';
     }
    $dynmodals .= $modal;
    
    
    
    
    $query9 = "SELECT classno,classdate,topic FROM courseclass WHERE courseid = $cid AND batchid = $bid AND teacherid = $id AND classdate < CURRENT_DATE ORDER BY classdate DESC LIMIT 1";
    $result9 = mysqli_query($connect, $query9);
    $dyntable = '<table cellpadding="20">';
    if(mysqli_num_rows($result9) == 0){
       
     }
     else{
    $row9 = mysqli_fetch_array($result9); 
    $weekno = $row9['classno'];   
    $dyndiv .= '<div id="Slider" class="slider slideup">
          <div id="Actual">
            <h5>Review your students for week - '.$weekno.' : </h5>
            <div class="divscroll">';
    $query6 = "SELECT studentid FROM studentcourse WHERE courseid = $cid AND batchid = $bid;";
    $result6 = mysqli_query($connect, $query6);
    $scount = 0;  
    $i = 0;
    while($row6 = mysqli_fetch_array($result6)){
      $sid = $row6['studentid'];
      $query10 = "SELECT id FROM performance WHERE studentid = $sid AND teacherid = $id AND courseid = $cid AND batchid = $bid AND classno = $weekno";
      $result10 = mysqli_query($connect, $query10);
      if(mysqli_num_rows($result10) != 0){
        
      }
      else{
      $scount++;
      $query7 = "SELECT name, photo FROM student WHERE id = $sid;";
      $result7 = mysqli_query($connect, $query7);
      $row7 = mysqli_fetch_array($result7);
      $sname = $row7['name'];
      $simg = '<img src="data:image/jpeg;base64,'.base64_encode($row7['photo'] ).'" style="margin:5px auto; border:solid black 1px;" width="150px" height="150px" alt="Your profile pic here" />'; 
      $dyncard = '<div class="card" style="width: 18rem; border:solid black 2px;">
                    '.$simg.'
                    <div class="card-body">
                      <center>
                        <h5 class="card-title">'.$sname.'</h5>
                      </center>
                      <form class="perfform">
                      <input type="hidden" name="classno" value="'.$weekno.'">
                      <input type="hidden" name="studentid" value="'.$sid.'">
                      <input type="hidden" name="teacherid" value="'.$id.'">
                      <input type="hidden" name="courseid" value="'.$cid.'">
                      <input type="hidden" name="batchid" value="'.$bid.'">
                      <textarea class="form-control perf" id="perf" name="perf" rows="3" placeholder="Write the student\'s performance review" style="margin-bottom:5px;" required></textarea>
                      <center><button type="submit" class="btn btn-primary">Submit</button></center>
                      </form>
                      <p class="revpara"></p>
                    </div>
                  </div>';
      if ($i % 4 == 0) { // if $i is divisible by our target number (in this case "3")
        $dyntable .= "<tr><td>" . $dyncard . "</td>";
      } else {
        $dyntable .= "<td>" . $dyncard . "</td>";
      }
      $i++;
//      echo $sname.'   '.$simg.'<br>';
      }
    }
       $dyntable .= "</tr></table>";
       $dyndiv .= $dyntable;
       if($scount == 0){
         $dyndiv .= '<center><h4 style="color:white; margin-top:50px;">All students reviewed for the week!</h4></center>';
       }
       $dyndiv .= ' </div>
          </div>
        </div>';
       
//    echo '<br><br>';
    }
    $dyndiv .= ' 
        </div>';
    
    $dynpage .= $dyndiv;
  }

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-grid.min.css">
  <!--  Make sure your always using the latest version of Bootstrap here-->
  <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
  <script href="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="teachdash.css">
  <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Neuton&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Archivo+Narrow&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet">
  <script src="jquery-3.4.1.min.js"></script>


  <script>
    $(document).ready(function(){  
      $('#updatepropic').click(function(){  
           var image_name = $('#profilepic').val();  
           if(image_name == '')  
           {  
                alert("Please Select Image");  
                return false;  
           }  
           else  
           {  
                var extension = $('#profilepic').val().split('.').pop().toLowerCase();  
                if(jQuery.inArray(extension, ['png','jpg','jpeg','JPG','JPEG','PNG']) == -1)  
                {  
                     alert('Invalid Image File');  
                     $('#profilepic').val('');  
                     return false;  
                }  
           }  
      });  
 });  
 </script>

</head>

<body class="stdbody">
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark navcustom fixed-top">
    <!-- Just an image -->

    <a class="navbar-brand" href="#">
      <img src="assets/logo.png" width="80px" height="50px">
    </a>


    <div class="collapse navbar-collapse justify-content-end collapsecustom" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Account
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
            <form method="post" action="teacherdashboard.php?id=<?php echo $id; ?>">
              <button type="submit" class="dropdown-item" name="signout">Sign Out</button>
              <hr>
              <button type="button" class="dropdown-item" name="uppass" data-toggle="modal" data-target="#updatepassmodal">Update Password</button>
              <hr>
              <button type="button" class="dropdown-item" name="uppic" data-toggle="modal" data-target="#updatepropicmodal">Update Profile Picture</button>
            </form>
          </div>
        </li>
      </ul>
    </div>
  </nav>
  <br><br>



  <div class="container-fluid nopadding">
    <div class="rowalt">
      <div class="pic_base" id="tprofilepic">
        <?php echo $profilepic; ?>
      </div>
      <div class="content_base">
        <h2 id='title'>
          <?php echo $name; ?>
        </h2>
        <br>
      </div>
    </div>





    <div class="container-fluid teachcourse">
      <p class="custpara">Your active batches - </p>
      <?php echo $dynpage; ?>

    </div>



  </div>

  <!--  Modals  -->
  <?php echo $dynmodals; ?>
  <div class="modal fade" id="updatepassmodal" tabindex="-1" role="dialog" aria-labelledby="updatepassmodal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Password</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="updatepassform">
            <div class="form-group">
              <input type="hidden" name="id" value="<?php echo $id; ?>">
              <label>Current Password</label>
              <input type="password" class="form-control" id="oldpass" name="oldpass" placeholder="Enter current password" required>
            </div>
            <div class="form-group">
              <label>Enter New Password</label>
              <input type="password" class="form-control" id="newpass" name="newpass" placeholder="Enter new Password" required>
            </div>
            <button type="submit" class="btn btn-primary" name="updatepass">Update</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="updatepropicmodal" tabindex="-1" role="dialog" aria-labelledby="updatepassmodal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Profile Picture</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p id="passerrors"></p>
          <form id="updatepropicform" enctype="multipart/form-data">
            <div class="form-group">
              <input type="hidden" name="id" value="<?php echo $id; ?>">
              <label for="exampleFormControlFile1">Upload your passport-size picture</label>
              <input type="file" class="form-control-file" name="profilepic" id="profilepic">
            </div>
            <button type="submit" class="btn btn-primary" name="updatepropic"  value="insert">Update</button>
          </form>
        </div>
      </div>
    </div>
  </div>


  <script>
    $(document).ready(function() {
      $(".clickable").click(function() {
        $(this).parent().parent().next(".slider").toggleClass("slidedown slideup");
      });

    });

  </script>
  <script>
    $(document).ready(function() {
      $(".perfform").submit(function(e) {
        e.preventDefault();
        var form = $(this);
        var url = "submitperf.php";
        $.ajax({
          type: "POST",
          url: url,
          data: form.serialize(),
          success: function(data) {
            var result = $.trim(data);
            if (result == "success") {
              form.replaceWith("<center><h6 style='color: green;'>Review Submitted</h6></center>");
            }
          }

        });

      });

    });

  </script>
  <script>
    $(document).ready(function() {
      $("form.addctopicform").submit(function(e) {
//        alert("form submitted");
        e.preventDefault();
        var form2 = $(this);
        var url2 = "addctopic.php";
        $.ajax({
          type: "POST",
          url: url2,
          data: form2.serialize(),
          success: function(data) {
            var result = $.trim(data);
            if (result == "success") {
              form2.replaceWith("<center><h6 style='color: green;'>Topic updated successfully</h6></center>");
            } else {
              form2.replaceWith("<center><h6 style='color: red;'>Error</h6></center>");
            }
          }

        });

      });

    });

  </script>
<!--
  <script>
    $(document).ready(function() {
      $("#updatepassform").submit(function(e) {
        alert("form submitted");
        e.preventDefault();
        var form2 = $(this);
        var url2 = "addctopic.php";
        $.ajax({
          type: "POST",
          url: url2,
          data: form2.serialize(),
          success: function(data) {
            var result = $.trim(data);
            if (result == "success") {
              form2.replaceWith("<center><h6 style='color: green;'>Topic updated successfully</h6></center>");
            } else {
              form2.replaceWith("<center><h6 style='color: green;'>Review Not Submitted</h6></center>");
            }
          }

        });

      });

    });

  </script>
-->

  <!--
  <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
-->

  <!--  Always Download latest version of Boostrap and add here-->
  <script src="bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js"></script>

</body>

</html>
