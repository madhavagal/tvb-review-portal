<?php
$connect = mysqli_connect("localhost","root","","tvb");
$query1 = "SELECT id,name,email,photo,phno from student";
$result1 = mysqli_query($connect, $query1);
$dyn_table = '<table cellpadding="20">
                <tr><th>id</th><th>Photo</th><th>Name</th><th>Email</th><th>Phone number</th></tr>';
while($row1 = mysqli_fetch_array($result1)){
  $id = $row1['id'];
  $name = $row1['name'];
  $email = $row1['email'];
  $photo = $row1['photo'];
  $phno = $row1['phno'];
  if(empty($photo))
    {
      $profilepic = '<img src="assets/default.png" width="50" height = "60"  alt="Your profile pic here" />';
    }
    else{
      $profilepic = '<img src="data:image/jpeg;base64,'.base64_encode($photo ).'" width="50" height = "60"  alt="Your profile pic here" />';
    }
  $namelink = '<a href="stdadmin.php?id='.$id.'">'.$name.'</a>';
  $dynrow = '<tr><td>'.$id.'</td><td>'.$profilepic.'</td><td>'.$namelink.'</td><td>'.$email.'</td><td>'.$phno.'</td></tr>';
  $dyn_table .= $dynrow;
}
$dyn_table .= '</table>';
?>
<?php
  include("adminheader.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-grid.min.css">
  <!--  Make sure your always using the latest version of Bootstrap here-->
  <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
  <script href="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="views.css">
  <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Neuton&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Archivo+Narrow&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet">
  <script src="jquery-3.4.1.min.js"></script>

</head>

<body class="stdbody">
  
  <div class="container-fluid">
    <center><?php echo $dyn_table; ?></center>
  </div>
  
  
  
<!--   <script src="bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js"></script>-->
</body>

</html>
