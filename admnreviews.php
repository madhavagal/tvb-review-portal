<?php
  include("adminheader.php");
?>
<?php
$connect = mysqli_connect("localhost","root","","tvb");
if(isset($_POST['viewreviews'])){
  $cid = $_POST['course'];
  $bid = $_POST['batch'];
  $query1 = "SELECT classno,classdate,teacherid FROM courseclass WHERE courseid = $cid AND batchid = $bid AND classdate < CURRENT_DATE ORDER BY classno DESC";
  $result1 = mysqli_query($connect, $query1);
  $dynpage = '';
  $query4 = "SELECT name FROM course WHERE id = $cid";
  $result4 = mysqli_query($connect, $query4);
  $row4 = mysqli_fetch_array($result4);
  $cname = $row4['name'];
  while($row1 = mysqli_fetch_array($result1)){
    $weekno = $row1['classno'];
    $tid = $row1['teacherid'];
    $query2 = "SELECT name FROM teacher WHERE id = $tid";
    $result2 = mysqli_query($connect, $query2);
    $row2 = mysqli_fetch_array($result2);
    $tname = $row2['name'];
    $query3 = "SELECT AVG(rating) AS avg FROM review WHERE classno = $weekno AND courseid = $cid AND batchid = $bid AND teacherid = $tid";
    $result3 = mysqli_query($connect, $query3);
    $row3 = mysqli_fetch_array($result3);
    $avgrating = round($row3['avg'], 2) ;
    $dynweek = '<div class="weekrev">

                  <h4>Week '.$weekno.' by '.$tname.'</h4>
                  <h6>Average rating - '.$avgrating.'</h6>
                  <br>
                  <div class="scrollable">

                  <table class="table table-bordered">
                    <thead class="thead-dark ">
                      <tr>
                        <th scope="col">Photo</th>
                        <th scope="col">Name</th>
                        <th scope="col">Suggestions</th>
                        <th scope="col">Feedback</th>
                        <th scope="col">Student Performance</th>
                      </tr>
                    </thead>
                    <tbody>';
    
      $query5 = "SELECT review.studentid, review.teacherid, review.courseid, review.batchid,            review.classno, review.feedback, review.suggestions, performance.progress FROM review LEFT OUTER JOIN performance ON review.courseid = performance.courseid AND review.classno = performance.classno AND review.batchid = performance.batchid AND review.studentid = performance.studentid AND review.teacherid = performance.teacherid WHERE review.courseid = $cid AND review.batchid = $bid AND review.classno = $weekno
        UNION
        SELECT performance.studentid, performance.teacherid, performance.courseid, performance.batchid, performance.classno, review.feedback, review.suggestions, performance.progress FROM review RIGHT OUTER JOIN performance ON review.courseid = performance.courseid AND review.classno = performance.classno AND review.batchid = performance.batchid AND review.studentid = performance.studentid AND review.teacherid = performance.teacherid WHERE performance.courseid = $cid AND performance.batchid = $bid AND performance.classno = $weekno";
    $result5 = mysqli_query($connect, $query5);
    while($row5 = mysqli_fetch_array($result5)){
      $sid = $row5['studentid'];
      if(empty($row5['suggestions'])){
        $sug = "Not added yet";
      }
      else{
        $sug = $row5['suggestions'];
      }
      if(empty($row5['feedback'])){
        $feed = "Not added yet";
      }
      else{
        $feed = $row5['feedback'];
      }
      if(empty($row5['progress'])){
        $perf = "Not added yet";
      }
      else{
        $perf = $row5['progress'];
      }
      $query6 = "SELECT name,photo FROM student WHERE id = $sid";
      $result6 = mysqli_query($connect, $query6);
      $row6 = mysqli_fetch_array($result6);
      $sname = $row6['name'];
      $sphoto = $row6['photo'];
      if(empty($sphoto))
        {
          $sprofilepic = '<img src="assets/default.png" width="50" height = "60"  alt="Your profile pic here" />';
        }
        else{
          $sprofilepic = '<img src="data:image/jpeg;base64,'.base64_encode($sphoto ).'" width="50" height = "60"  alt="Your profile pic here" />';
        }
      $namelink = '<a href="stdadmin.php?id='.$sid.'">'.$sname.'</a>';
      $dynrow = '  <tr>
              <td>'.$sprofilepic.'</td>
              <td>'.$sname.'</td>
              <td>'.$sug.'</td>
              <td>'.$feed.'</td>
              <td>'.$perf.'</td>
            </tr>';
      
      $dynweek .= $dynrow;
    }
    $dynweek .= '      </tbody>
        </table>

      </div>

    </div>';
    $dynpage .= $dynweek.'<br><hr><br>';
  }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-grid.min.css">
  <!--  Make sure your always using the latest version of Bootstrap here-->
  <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
  <script href="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="admrev.css">
  <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Neuton&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Archivo+Narrow&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet">
  <script src="jquery-3.4.1.min.js"></script>

</head>

<body class="stdbody">

  <div class="container-fluid">

    <h3>Reviews for <?php echo $cname; ?> - Batch <?php echo $bid ?></h3>
    <hr style="height: 2px; border: none; color:#333; background-color:#333;">
    <br><br>

    <?php echo $dynpage; ?>
    
  </div>


  <!--   <script src="bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js"></script>-->
</body>

</html>
