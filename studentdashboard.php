<?php  
session_start();
$connect = mysqli_connect("localhost", "root", "", "tvb");
$id=$_SESSION['id'];
$query1 = "SELECT name,email,photo from student WHERE id=$id";
$result1 = mysqli_query($connect, $query1);
$row1 = mysqli_fetch_array($result1);
$name = $row1['name'];
$email = $row1['email'];
$photo = $row1['photo'];
$profilepic = '<img src="data:image/jpeg;base64,'.base64_encode($photo ).'" width="150" height = "190" id="profile_pic" alt="Your profile pic here" />';
if($email != $_SESSION['email']){
   echo '<script>alert("Acess denied. Log in again!")</script>';
   echo "<script> location.href='login.php'; </script>";
       exit;
}
$query2 = "SELECT courseid, batchid FROM studentcourse WHERE studentid = $id";
$result2 = mysqli_query($connect, $query2);
$i = 0;
$dyn_table = "<table cellpadding=\"20\">"; 
 while($row2 = mysqli_fetch_array($result2))  
 {
   $cid = $row2['courseid'];
   $bid = $row2['batchid'];
//   echo "Course id - $cid Batch id- $bid";
   $query3 = "SELECT photo,name FROM course WHERE id = $cid";
   $result3 = mysqli_query($connect, $query3);
   $row3 = mysqli_fetch_array($result3);
   $cname = $row3['name'];
   $image = $row3['photo'];
//   echo ' course name - '. $cname.'photo - <img src="data:image/jpeg;base64,'.base64_encode($image ).'" height="200" width="200" class="img-thumnail" />  ';
   $query4 = "SELECT sdate,enddate, active FROM batches WHERE batchid = $bid AND courseid = $cid";
   $result4 = mysqli_query($connect, $query4);
   $row4 = mysqli_fetch_array($result4);
   $enddate = $row4['enddate'];
   $sdate = $row4['sdate'];
   $active = $row4['active'];
   $tdate = date("Y-m-d");
//   echo "enddate -  $enddate active - $active todays date - $tdate <br>";
     
   $msg;
   if ($tdate > $enddate and $active != 1)
   {
     $msg = "<p class=\"completed\">Course Completed</p>";
   }
   elseif ($tdate < $sdate and $active != 1)
   {
     $msg = "<p class=\"notstarted\">Course not started</p>";
   }
   else{
     $query5 = "SELECT classno,classdate,topic FROM courseclass WHERE courseid = $cid AND batchid = $bid AND classdate >= CURRENT_DATE ORDER BY classdate ASC LIMIT 1";
     $result5 = mysqli_query($connect, $query5);
     if(mysqli_num_rows($result5) == 0){
       $msg = "<p class=\"notcompleted\">Upcoming class not added yet</p>";
     }
     else{
       $row5 = mysqli_fetch_array($result5);
     
     $update = date("d-m-Y",strtotime($row5['classdate']));
     $topic = $row5['topic'];
     $msg = "<p class=\"notcompleted\">Upcoming Class on $update <br> Topic - $topic</p>";
     }

   }
//   echo "$msg ";
   $img = '<img src="data:image/jpeg;base64,'.base64_encode($image ).'" height="150px" width="319px" class="img-thumnail" />';
   $div = "<div class=\"card arrow\" style=\"width: 20rem; height:20rem;\">
            $img
            <div class=\"card-body\">
            <h3 class=\"card-title custtitle\">$cname - Batch $bid</h3>
            <p class=\"card-text\">$msg </p>
            </div>
            </div>";
      if ($i % 4 == 0) { // if $i is divisible by our target number (in this case "3")
        $dyn_table .= "<tr><td>" . $div . "</td>";
      } else {
        $dyn_table .= "<td>" . $div . "</td>";
      }
      $i++;
 }
$dyn_table .= "</tr></table>";
?>

<?php
  if(isset($_POST['reviewsubmit']))
  {
    $classno = $_POST['classno'];
    $studentid = $_POST['studentid'];
    $teacherid = $_POST['teacherid'];
    $couid = $_POST['courseid'];
    $batid = $_POST['batchid'];
    $rating = $_POST['star'];
    $feedback = $_POST['feedback'];
    $sugg = $_POST['suggestions'];
    $query10 = "INSERT INTO review(classno,studentid,teacherid,courseid,batchid,rating,feedback,suggestions) VALUES('$classno','$studentid','$teacherid','$couid','$batid','$rating','$feedback','$sugg')";
    if(mysqli_query($connect, $query10))  
      {  
           echo '<script>alert("Success")</script>';  
           header("Refresh:0");
            $_POST["reviewsubmit"] = '';
      }  
      else
      {
          echo '<script>alert("Error")</script>'; 
      }
    
  }
?>
<?php
  $query11 = "SELECT courseid, batchid FROM studentcourse WHERE studentid = $id";
  $result11 = mysqli_query($connect, $query11);
  $dyntable2 = ""; 
  while($row11 = mysqli_fetch_array($result11))  
  {
    $courid = $row11['courseid'];
    $batcid = $row11['batchid'];
    $query12 = "SELECT classno, topic, teacherid, classdate FROM courseclass WHERE courseid = $courid AND batchid = $batcid AND classdate < CURRENT_DATE ORDER BY classdate DESC LIMIT 1";
    $result12 = mysqli_query($connect, $query12);
    $row12 = mysqli_fetch_array($result12);
    $query13 = "SELECT name, photo FROM course WHERE id = $courid";
    $result13 = mysqli_query($connect, $query13);
    $row13 = mysqli_fetch_array($result13);
    $clasno = $row12['classno'];
    $top = $row12['topic'];
    $teacid = $row12['teacherid'];
    $cldat = $row12['classdate'];
    $pho = $row13['photo'];
    $courname = $row13['name'];
    $query14 = "SELECT name FROM teacher WHERE id = $teacid";
    $result14 = mysqli_query($connect, $query14);
    $row14 = mysqli_fetch_array($result14);
    $teacname = $row14['name'];
    $courimg = '<img src="data:image/jpeg;base64,'.base64_encode($pho ).'" height="100%" width="100%" class="img-thumnail" />'; 
    $revdate = date("d-m-Y",strtotime($cldat));
    $query15 = "SELECT id FROM review WHERE studentid = $id AND teacherid = $teacid AND courseid = $courid AND batchid = $batcid AND classno = $clasno";
    $result15 = mysqli_query($connect, $query15);
    if(mysqli_num_rows($result15) != 0){
       $dyndiv = '<p>Review Already Submitted! </p>';
     }
     else{
    $revhead = 'Week '.$clasno.'- '.$revdate.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; By '.$teacname.' ';
    $dyndiv = '<div class="row">
      <div class="col-md-7">
        '.$courimg.'
      </div>
      <div class="col-md-5">
        <h2 class="coursetitlereview">'.$courname.'</h2>
        <h3>'.$revhead.'</h3>
        <hr style="height: 2px; border: none; color:#333; background-color:#333;">
        <br>
        <form action="studentdashboard.php?id='.$id.'" method="post">
          <input type="hidden" name="classno" value="'.$clasno.'">
          <input type="hidden" name="studentid" value="'.$id.'">
          <input type="hidden" name="teacherid" value="'.$teacid.'">
          <input type="hidden" name="courseid" value="'.$courid.'">
          <input type="hidden" name="batchid" value="'.$batcid.'">
          <div class="form-group">
            <div class="row">
              <div class="col-md-2">
                <p style="padding-top:10px;">Rating</p>
              </div>
              <div class="col-md-10">
                <div class="txt-center">
                  <div class="rating">
                    <input id="star5" name="star" type="radio" value="5" class="radio-btn hide" />
                    <label for="star5" style="font-size:200%;">&star;</label>
                    <input id="star4" name="star" type="radio" value="4" class="radio-btn hide" />
                    <label for="star4" style="font-size:200%;">☆</label>
                    <input id="star3" name="star" type="radio" value="3" class="radio-btn hide" />
                    <label for="star3" style="font-size:200%;">☆</label>
                    <input id="star2" name="star" type="radio" value="2" class="radio-btn hide" />
                    <label for="star2" style="font-size:200%;">☆</label>
                    <input id="star1" name="star" type="radio" value="1" class="radio-btn hide" />
                    <label for="star1" style="font-size:200%;">☆</label>
                    <div class="clear"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <p>Feedback</p>
            <textarea class="form-control" name="feedback" rows="3" placeholder="How was the class?"></textarea>
          </div>
          <div class="form-group">
            <p>Suggestions</p>
            <textarea class="form-control" name="suggestions" rows="3" placeholder="How can we improve the class experience?"></textarea>
          </div>
          <button type="submit" name="reviewsubmit" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>';
     }
    $dyntable2 .= $dyndiv.'<br><hr style="height:0.2px; color:#333; background-color:#333;"><br>';
  }

?>
<?php
  if(isset($_POST['signout'])){
    session_destroy();
	 unset($_SESSION['email']);
    header('location: login.php');
  }
?>
<?php
  if(isset($_POST['updatepass'])){
    
    $oldpass = $_POST['oldpass'];
    $newpass = $_POST['newpass'];
    $query16 = "SELECT password FROM student WHERE id = $id";
    $result16 = mysqli_query($connect, $query16);
    $row16 = mysqli_fetch_array($result16);
    $curpass = $row16['password'];
    if($oldpass == $newpass and $oldpass == $curpass ){
      echo "<script>alert('New password can not be same as old password')</script>";
    } 
    elseif($oldpass != $curpass){
      echo "<script>alert('Enter the correct password')</script>";
    }
	 else{
        $query17 = "UPDATE student SET `password` = '$newpass' WHERE id = $id";
        if(mysqli_query($connect, $query17) == TRUE){
          echo "<script>alert('Password updated successfully')</script>";
        }
        else{
          echo "<script>alert('Please try again')</script>";
        }
      }
    }
?>
<?php 
  if(isset($_POST["updatepropic"]))  
 {  
      $file = addslashes(file_get_contents($_FILES["profilepic"]["tmp_name"]));  
//      $query = "INSERT INTO tbl_images(name) VALUES ('$file')";
      $query18 = "UPDATE student SET `photo` = '$file' WHERE id = $id";
      if(mysqli_query($connect, $query18))  
      {  
           echo '<script>alert("Profile ")</script>';  
      }  
 }  
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-grid.min.css">
  <!--  Make sure your always using the latest version of Bootstrap here-->
  <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
  <script href="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="stddash.css">
  <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Neuton&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Archivo+Narrow&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet">
  <style>
    .txt-center {
  text-align: center;
}

.hide {
  display: none;
}

.clear {
  float: none;
  clear: both;
}

.rating {
  width: 200px;
  unicode-bidi: bidi-override;
  direction: rtl;
  text-align: left;
  position: relative;
}

.rating > label {
  float: right;
  display: inline;
  padding: 0;
  margin: 0;
  position: relative;
  width: 1.1em;
  cursor: pointer;
  color: #000;
}

.rating > label:hover,
.rating > label:hover ~ label,
.rating > input.radio-btn:checked ~ label {
  color: transparent;
}

.rating > label:hover:before,
.rating > label:hover ~ label:before,
.rating > input.radio-btn:checked ~ label:before,
.rating > input.radio-btn:checked ~ label:before {
  content: "\2605";
  position: absolute;
  left: 0;
  color: #FFD700;
}

  </style>
  <script>
    $(document).ready(function(){  
      $('#updatepropic').click(function(){  
           var image_name = $('#profilepic').val();  
           if(image_name == '')  
           {  
                alert("Please Select Image");  
                return false;  
           }  
           else  
           {  
                var extension = $('#profilepic').val().split('.').pop().toLowerCase();  
                if(jQuery.inArray(extension, ['png','jpg','jpeg','JPG','JPEG','PNG']) == -1)  
                {  
                     alert('Invalid Image File');  
                     $('#profilepic').val('');  
                     return false;  
                }  
           }  
      });  
 });  
 </script>

</head>

<body class="stdbody">
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark navcustom fixed-top ">
    <!-- Just an image -->

    <a class="navbar-brand" href="#">
      <img src="assets/logo.png" width="80px" height="50px">
    </a>


    <div class="collapse navbar-collapse justify-content-end collapsecustom" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Account
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
            <form method="post" action="studentdashboard.php?id=<?php echo $id; ?>">
              <button type="submit" class="dropdown-item" name="signout">Sign Out</button>
              <hr>
              <button type="button" class="dropdown-item" name="uppass" data-toggle="modal" data-target="#updatepassmodal">Update Password</button>
              <hr>
              <button type="button" class="dropdown-item" name="uppic" data-toggle="modal" data-target="#updatepropicmodal">Update Profile Picture</button>
            </form>
          </div>
        </li>
      </ul>
    </div>
  </nav>
  <br><br>
  <div class="container-fluid nopadding">
    <div class="rowalt">
      <div class="pic_base">
        <?php echo $profilepic ?>
      </div>
      <div class="content_base">
        <h2 id='title'>
          <?php echo $name; ?>
        </h2>
        <br>
      </div>
    </div>

    <p class="custpara">Your Courses - </p>
    <div class="container-fluid">
      <?php echo $dyn_table; ?>
    </div>
    <p class="custpara">Review your last class -</p>
    <div class="container-fluid">
      <?php echo $dyntable2; ?>
    </div>

  </div>

  <!--  Modals  -->
  <div class="modal fade" id="updatepassmodal" tabindex="-1" role="dialog" aria-labelledby="updatepassmodal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Password</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="post" action="studentdashboard.php?id=<?php echo $id; ?>">
            <div class="form-group">
              <label>Current Password</label>
              <input type="password" class="form-control" name="oldpass" placeholder="Enter current password" required>
            </div>
            <div class="form-group">
              <label>Enter New Password</label>
              <input type="password" class="form-control" name="newpass" placeholder="Enter new Password" required>
            </div>
            <button type="submit" class="btn btn-primary" name="updatepass">Update</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="updatepropicmodal" tabindex="-1" role="dialog" aria-labelledby="updatepassmodal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Profile Picture</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="post" action="studentdashboard.php?id=<?php echo $id; ?>" enctype="multipart/form-data">
            <div class="form-group">
              <label for="exampleFormControlFile1">Upload your passport-size picture</label>
              <input type="file" class="form-control-file" name="profilepic" id="profilepic">
            </div>
            <button type="submit" class="btn btn-primary" name="updatepropic" id="updatepropic" value="insert">Update</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
  <!--  Always Download latest version of Boostrap and add here-->
  <script src="bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js"></script>

</body>

</html>
