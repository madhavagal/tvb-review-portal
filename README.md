# Overview
The Review Portal enables the students to review the instructors based on their performance, quality of teaching and they can give any feedback if necessary. The instructors can also review each student after every week to keep track of their progress. The Admin can monitor the instructor’s performance as well as the student’s feedback and progress. 

# Goals
1.	Students should be able to give feedback of the course instructor on a weekly basis.
2.	Teachers will be able to assess the students and monitor their progress.
3.	The Admin will be able to add/modify courses and course instructors as and when necessary.
4.	The Admin will be able to add new students for new batches as well as generate their login credentials.
5.	The Admin should get an organised view of each active course which includes the  course instructor’s feedback as well as the progress of each student in each batch. 

# Milestones
1.	Wireframes
2.	Designs
3.	Development (Front and Back-end)
4.	Testing and Feedback
5.	Go live


#	Functionality

1. The admin provides the login credentials for each student and instructor. 
2. The students can use their credentials to log in and view the courses they are enrolled in. The student dashboard will also    show their upcoming classes and after a class, the student can provide review for the instructor as well as ask any doubts    or give suggestions about upcoming classes.
3. The instructors can log in to  view the courses they teach and review each student’s progress.
4. The admin can mark a course as active or inactive, add new courses and students to each course.
5. The admin can assign instructors to the active courses.
6. The admin can view each instructor’s feedback for students and the review each student gave about the instructor.

# Hosting
The website will be hosted using Amazon Web Services. 


